# Spreadsheet with Vertical Headers and Data Processing Demo

This is the code used for the tutorial "Processing Spreadsheets with Vertical Headers and Data in Laravel". 

## Prerequisites

1. Composer
2. A local server such as Wampserver or Laragon
3. MongoDB Extension for PHP
4. MongoDB Compass to look at the data (optional)
## Setting it up in your local environment

1. Once you have the prerequisites installed and you have cloned the project, go to the folder where the project files are and open up a terminal. Type "composer install"
2. If you have not added a MySQL table called "vertical_spreadsheet_demo", do so now. Once done, type in "php artisan migrate:fresh".
3. Have fun.