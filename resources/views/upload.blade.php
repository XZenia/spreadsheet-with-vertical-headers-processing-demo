@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Upload Spreadsheet') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="document" class="form-label">Vertical Spreadsheet</label>
                            <input type="file" class="form-control" name="document" accept=".xlsx,.xls" required>
                        </div>
                        <div class="col-auto my-1">
                            <button type="submit" class="btn btn-primary btn-submit">Submit Document</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
