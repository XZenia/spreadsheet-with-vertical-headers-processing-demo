<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentRequest;
use App\Imports\VerticalSheetImport;
use App\Models\Document;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("upload");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentRequest $request)
    {
        $spreadsheet_data = $this->processVerticalLayoutSpreadsheet($request->document, 1);
        $new_document = Document::create([
            'user_data' => $spreadsheet_data
        ]);

        return redirect()->route('document.index');
    }


    /**
     * Processes spreadsheets row by row. This is for spreadsheets where the headers are in a vertical position.
     * Returns an array with every row value.
     *
     * @param  string $filepath
     * @param  Integer $header_column_index
     * @return Array
     *
     *
     */
    public function processVerticalLayoutSpreadsheet(string $filepath, $header_column_index = 1){
        try{
            $excel_rows = Excel::toArray(new VerticalSheetImport(), $filepath)["USERDATA"];
        } catch(Exception $exception) {
            throw new Exception("Worksheet with the name USERDATA was not found. Please reupload the spreadsheet with this worksheet name where the equipment data is saved.");
        }

        $all_processed_columns = [];
        $heading_column = [];
        $number_of_data = 0;

        //Gets heading row and removes row data before it to avoid conflicting with later processes.
        foreach($excel_rows as $excel_row_key => $excel_row){
            $heading_column[] = Str::slug($excel_row[$header_column_index], "_");
            for($counter = 0; $counter <= $header_column_index; $counter++){
                unset($excel_rows[$excel_row_key][$counter]);
            }

            $excel_rows[$excel_row_key] = array_values($excel_rows[$excel_row_key]);
        }

        $number_of_data = count($excel_rows);
        foreach($excel_rows as $excel_row_key => $excel_row){
            for($counter = 0; $counter < $number_of_data; $counter++){
                $all_processed_columns[$counter][$heading_column[$excel_row_key]] = $excel_row[$counter];
            }
        }

        $all_processed_columns = array_filter(array_map(function($column){
            if (!empty(array_filter($column))){
                return $column;
            }
        }, $all_processed_columns));

        //Filters out all of the empty columns to get only the relevant data.
        foreach($all_processed_columns as $key => $column){
            if (!isset($column[Str::slug("ID_No", "_")])){
                unset($all_processed_columns[$key]);
            }
        }

        $all_processed_columns = array_values($all_processed_columns);

        return $all_processed_columns;
    }

}
