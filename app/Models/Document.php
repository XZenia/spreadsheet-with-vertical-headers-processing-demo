<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $collection = 'documents';
    protected $connection = 'mongodb';

    protected $guarded    = ['_id'];
    protected $fillable   = ['user_data'];
    protected $dates      = ['created_at', 'updated_at'];
}
